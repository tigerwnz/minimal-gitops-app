FROM ruby:3-alpine

WORKDIR /app
COPY Gemfile Gemfile.lock /app
RUN bundle install

COPY ./ /app

EXPOSE 8080
CMD ["bundle", "exec", "ruby", "./src/server.rb"]
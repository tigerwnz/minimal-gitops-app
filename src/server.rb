# frozen_string_literal: true

require 'webrick'

server = WEBrick::HTTPServer.new(Port: 8080)

server.mount_proc '/' do |_, response|
  response.body = "Hello from #{ENV['ENVIRONMENT']}! V2"
end

server.start
